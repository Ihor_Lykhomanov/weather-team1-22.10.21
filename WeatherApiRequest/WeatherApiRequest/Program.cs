﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WeatherApiRequest;

namespace RequestBinExample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            int millisecondsDelay = 2000;
            ActionSelection.PostOrGetInfo(millisecondsDelay);
            await Task.Delay(millisecondsDelay + 500);
        }
    }
}