﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace WeatherApiRequest
{
    public class ApiResponse
    {
        public static async Task MakeRequest(string date, int millisecondsDelay)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetStringAsync("http://requestbin.net/r/gfvv4h7s?inspect");
            string encodedString = HttpUtility.HtmlDecode(response);

            string[] result = encodedString.Split("POST PARAMETERS");
            string answer = "";

            for (int i = 0; i < result.Length; i++)
            {
                if (result[i].Contains(date))
                {
                    answer = result[i];
                    break;                
                }
            }
            if (answer == "")
            {
                Console.WriteLine("Oops. This date is not found...");
                return;
            }
            var matches = new Regex(@"<strong>(?<key>\w+):<\/strong> (?<value>[a-zA-Z0-9\/]+)<\/p>").Matches(answer);
            var groupText = "";
            for (int i = 0; i < matches.Count; i++)
            {
                groupText = matches[i].Groups["Key"].ToString();
                Console.WriteLine(groupText);
            }
        }
    }
}
