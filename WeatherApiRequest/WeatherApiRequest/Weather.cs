﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApiRequest
{
    public class Weather
    {
        public static void FillWeatherData()
        {
            Console.WriteLine("Enter the date of weather in format dd.mm.yy");
            string date = Console.ReadLine();
            Console.WriteLine("Enter the temperature in degrees celsius for the entered date");
            string temperature = Console.ReadLine();
            Console.WriteLine("Enter the pressure for the entered date");
            string pressure = Console.ReadLine();
            Console.WriteLine("Enter the humidity for the entered date");
            string humidity = Console.ReadLine();
            Console.WriteLine("Enter the speed of wind for the entered date");
            string windSpeed = Console.ReadLine();
            Console.WriteLine("Enter the probability of precipitation for the entered date");
            string propabilityOfPrecipitation = Console.ReadLine();

            ApiRequest.PostWeather(date, temperature, pressure, humidity, windSpeed, propabilityOfPrecipitation);
        }
        public static async Task GetWeather(int millisecondDelay)
        {
            Console.WriteLine("Enter the date about which you want to know the weather forecast \n" +
                "in format dd.mm.yy");
            string date = Console.ReadLine();

            int millisecondsDelay = 2000;
            await ApiResponse.MakeRequest(date, millisecondsDelay);
            await Task.Delay(millisecondsDelay + 500);
        }
    }
}
