﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApiRequest
{
    class ApiRequest
    {
        public static void PostWeather(string date, string temperature, string pressure, string humidity, string windSpeed, string propabilityOfPrecipitation)
        {
            RestClient weather = new RestClient("https://requestbin.net/r/ap7y4us9")
            {
                Timeout = 30000
            };
            RestRequest requestWeather = new RestRequest(Method.POST);
            requestWeather.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            requestWeather.RequestFormat = DataFormat.Json;

            requestWeather.AddParameter("Date", date);
            requestWeather.AddParameter("Temperature, C", temperature);
            requestWeather.AddParameter("Pressure, mom", pressure);
            requestWeather.AddParameter("Humidity, %", humidity);
            requestWeather.AddParameter("Wind, m/s", windSpeed);
            requestWeather.AddParameter("Probability of precipitation, %", propabilityOfPrecipitation);

            weather.Execute(requestWeather);
        }
    }
}
