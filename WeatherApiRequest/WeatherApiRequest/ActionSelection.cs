﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApiRequest
{
    class ActionSelection
    {
        public static async Task PostOrGetInfo(int num)
        {
            Console.WriteLine("Press 1 to post info about weather" +
                "\nPress 2 to get info about weather");
            string action = Console.ReadLine();
            bool isActionCorrect = true;

            while (isActionCorrect)
            {
                switch (action)
                {
                    case "1":
                        {
                            isActionCorrect = false;
                            Weather.FillWeatherData();
                        }
                        break;
                    case "2":
                        {
                            isActionCorrect = false;
                            int millisecondDelay = 2000;
                            Weather.GetWeather(millisecondDelay);
                            await Task.Delay(millisecondDelay + 500);
                        }
                        break;
                    default: Console.WriteLine("Invalid data. Try again");
                        break;
                }
            }
        }
    }
}
